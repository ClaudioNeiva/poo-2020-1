package br.ucsal.bes20201.poo.aula04.domain;

public class Aluno {

	// FIXME A defini��o dos atributos como visibilidade p�blica � PROVIS�RIA. Nas
	// pr�ximas aulas, modificaremos para privado e criaremos m�todos para realizar
	// atualiza��o e consulta dos dados dos atributos quando os mesmos forem
	// privados.

	// TODO Pesquisar sobre atributos e m�todos de classe (ou est�ticos) e de
	// inst�ncia.

	public Integer matricula;

	public String nome;

	public String email;

	// FIXME Essa implementa��o � MUITO ruim, e FALHA em determinadas situa��es.
	// Vamos substitu�-la por uma melhor numa pr�xima aula.
	@Override
	public boolean equals(Object object) {
		Aluno aluno = (Aluno) object;
		if (!matricula.equals(aluno.matricula)) {
			return false;
		}
		if (!nome.equals(aluno.nome)) {
			return false;
		}
		if (!email.equals(aluno.email)) {
			return false;
		}
		return true;
	}
}
