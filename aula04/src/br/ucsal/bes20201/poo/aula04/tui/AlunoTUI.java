package br.ucsal.bes20201.poo.aula04.tui;

import java.util.Scanner;

import br.ucsal.bes20201.poo.aula04.business.AlunoBO;
import br.ucsal.bes20201.poo.aula04.domain.Aluno;

public class AlunoTUI {

	private static Scanner sc = new Scanner(System.in);

	public static void cadastrarAluno() {
		Integer matricula;
		String nome;
		String email;

		System.out.println("########### CADASTRO ALUNO ###########");
		System.out.println("Informe os dados do aluno:");

		System.out.print("Matr�cula:");
		matricula = sc.nextInt();
		sc.nextLine();

		System.out.print("Nome:");
		nome = sc.nextLine();

		System.out.print("Email:");
		email = sc.nextLine();

		Aluno aluno = new Aluno();
		aluno.matricula = matricula;
		aluno.nome = nome;
		aluno.email = email;

		if (AlunoBO.cadastrarAluno(aluno)) {
			System.out.println("Aluno cadastrado com sucesso.");
		} else {
			System.out.println("Ocorreu um erro. N�o foi poss�vel cadastrar o aluno.");
		}
	}
}
