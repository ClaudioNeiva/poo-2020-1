package br.ucsal.bes20201.poo.aula04.exemplo;

import br.ucsal.bes20201.poo.aula04.domain.Aluno;

public class Exemplo {

	public static void main(String[] args) {

		Aluno aluno1 = new Aluno();
		aluno1.matricula = 123;
		aluno1.nome = "Claudio";
		aluno1.email = "claudio@ucsal.br";

		Aluno aluno2 = new Aluno();
		aluno2.matricula = 123;
		aluno2.nome = "Claudio";
		aluno2.email = "claudio@ucsal.br";

		// A pergunta n�o � "Os objetos s�o iguais?". A pergunta que est� sendo feita �
		// "As vari�veis apontam para a mesma inst�ncia?"
		if (aluno1 == aluno2) {
			System.out.println("Apontam para a mesma inst�ncia.");
		} else {
			System.out.println("N�O apontam para a mesma inst�ncia.");
		}
		System.out.println("aluno1=" + aluno1);
		System.out.println("aluno2=" + aluno2);

		if (aluno1.equals(aluno2)) {
			System.out.println("Apontam para objetos iguais.");
		} else {
			System.out.println("Apontam para objetos diferentes.");
		}
	}

}
