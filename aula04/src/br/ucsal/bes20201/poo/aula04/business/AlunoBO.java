package br.ucsal.bes20201.poo.aula04.business;

import br.ucsal.bes20201.poo.aula04.domain.Aluno;
import br.ucsal.bes20201.poo.aula04.persistence.AlunoDAO;

public class AlunoBO {

	public static boolean cadastrarAluno(Aluno aluno) {
		if (validarAluno(aluno)) {
			AlunoDAO.inserir(aluno);
			return true;
		} else {
			return false;
		}
	}

	// FIXME Quando estudarmos exceptions, vamos mudar este c�digo.
	private static boolean validarAluno(Aluno aluno) {
		if (aluno.matricula == null) {
			return false;
		}
		if (aluno.nome == null || aluno.nome.trim().isEmpty()) {
			return false;
		}
		return true;
	}

}
