package br.ucsal.bes20201.poo.aula04.persistence;

import br.ucsal.bes20201.poo.aula04.domain.Aluno;

public class AlunoDAO {

	private static final int QTD_MAX_ALUNOS = 1000;

	private static Aluno[] alunos = new Aluno[QTD_MAX_ALUNOS];
	private static int qtdAlunos = 0;

	public static void inserir(Aluno aluno) {
		alunos[qtdAlunos] = aluno;
		qtdAlunos++;
	}

}
