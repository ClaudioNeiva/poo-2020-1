package br.ucsal.bes20201.poo.aula04.banco1.tui;

import java.util.Scanner;

import br.ucsal.bes20201.poo.aula04.banco1.business.ContaCorrenteBO;
import br.ucsal.bes20201.poo.aula04.banco1.domain.ContaCorrente;

public class MovimentacaoTUI {

	private static Scanner sc = new Scanner(System.in);

	public static void sacar() {
		String numAgencia;
		String numConta;
		Double valor;

		System.out.println("Informe o número da agência:");
		numAgencia = sc.nextLine();
		System.out.println("Informe o número da conta:");
		numConta = sc.nextLine();
		System.out.println("Informe o valor:");
		valor = sc.nextDouble();
		sc.nextLine();

		ContaCorrente contaCorrente = ContaCorrenteBO.obter(numAgencia, numConta);
		if (contaCorrente == null) {
			System.out.println("Conta corrente não cadastrada.");
		} else {
			if (contaCorrente.sacar(valor)) {
				System.out.println("Saque realizado. com sucesso na conta de " + contaCorrente.getNomeCorrentista());
			} else {
				System.out.println("Saldo insuficiente!");
			}
		}

	}

	public static void depositar() {

	}

	public static void consultarSaldo() {

	}

	static void emitirExtrato() {

	}
}
