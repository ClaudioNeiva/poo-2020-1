package br.ucsal.bes20201.poo.aula04.banco1.domain;

public class ContaCorrenteExemploConstrutores {

	private String numAgencia;

	private String numConta;

	private String nomeCorrentista;

	private Double saldo;

	public ContaCorrenteExemploConstrutores() {
		System.out.println("ContaCorrente()");
		saldo = 0d;
	}
	
	public ContaCorrenteExemploConstrutores(String numAgencia, String numConta) {
		this();
		System.out.println("ContaCorrente(String numAgencia, String numConta)");
		this.numAgencia = numAgencia;
		this.numConta = numConta;
	}
	
	public ContaCorrenteExemploConstrutores(String numAgencia, String numConta, String nomeCorrentista) {
		this(numAgencia, numConta);
		System.out.println("ContaCorrente(String numAgencia, String numConta, String nomeCorrentista)");
		this.nomeCorrentista = nomeCorrentista;
	}
}
