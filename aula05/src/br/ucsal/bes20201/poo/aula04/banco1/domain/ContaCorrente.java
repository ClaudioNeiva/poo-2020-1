package br.ucsal.bes20201.poo.aula04.banco1.domain;

public class ContaCorrente {

	private static final double SALDO_INICIAL = 0d;

	private String numAgencia;

	private String numConta;

	private String nomeCorrentista;

	private Double saldo;

	public ContaCorrente(String numAgencia, String numConta, String nomeCorrentista) {
		this.numAgencia = numAgencia;
		this.numConta = numConta;
		this.nomeCorrentista = nomeCorrentista;
		saldo = SALDO_INICIAL;
	}

	// FIXME Modificar para void e tratar a falta de saldo como uma exce��o.
	public Boolean sacar(Double valor) {
		if (saldo >= valor) {
			saldo -= valor;
			return true;
		}
		// FIXME Lan�ar uma exce��o aqui!
		return false;
	}

	public String getNumAgencia() {
		return numAgencia;
	}

	public String getNumConta() {
		return numConta;
	}

	public String getNomeCorrentista() {
		return nomeCorrentista;
	}

	public void setNomeCorrentista(String nomeCorrentista) {
		// Posso escreve aqui regras de neg�cio que validam o nomeCorrentista antes de
		// atualizar o atributo.
		this.nomeCorrentista = nomeCorrentista;
	}
}
