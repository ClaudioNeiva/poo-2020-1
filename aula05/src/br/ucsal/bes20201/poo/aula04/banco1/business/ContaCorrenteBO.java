package br.ucsal.bes20201.poo.aula04.banco1.business;

import br.ucsal.bes20201.poo.aula04.banco1.domain.ContaCorrente;

public class ContaCorrenteBO {

	// FIXME Esta classe est� com responsabilidades da persist�ncia. Por enquando
	// vamos deixar assim e, na pr�xima aula, faremos essa refatora��o.

	private static final int QTD_MAX = 1000;

	private static ContaCorrente[] contasCorrente = new ContaCorrente[QTD_MAX];
	private static int qtd = 0;

	public static void inserir(ContaCorrente contaCorrente) {
		if (obter(contaCorrente.getNumAgencia(), contaCorrente.getNumConta()) == null) {
			contasCorrente[qtd] = contaCorrente;
			qtd++;
		} else {
			// FIXME Comandos de entrada e sa�da n�o deveriam estar presentes na camada de
			// neg�cio!
			System.out.println("Conta já cadastrada!");
		}
	}

	public static ContaCorrente obter(String numAgencia, String numConta) {
		for (int i = 0; i < qtd; i++) {
			if (numAgencia.equals(contasCorrente[i].getNumAgencia()) && numConta.equals(contasCorrente[i].getNumConta())) {
				return contasCorrente[i];
			}
		}
		// FIXME Lan�ar uma exce��o aqui!
		return null;
	}

}
