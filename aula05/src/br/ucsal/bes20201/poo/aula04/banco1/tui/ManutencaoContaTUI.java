package br.ucsal.bes20201.poo.aula04.banco1.tui;

import java.util.Scanner;

import br.ucsal.bes20201.poo.aula04.banco1.business.ContaCorrenteBO;
import br.ucsal.bes20201.poo.aula04.banco1.domain.ContaCorrente;

public class ManutencaoContaTUI {


	private static Scanner sc = new Scanner(System.in);

	public static void abrir() {
		String numAgencia;
		String numConta;
		String nomeCorrentista;

		System.out.println("Informe o número da agência:");
		numAgencia = sc.nextLine();
		System.out.println("Informe o número da conta:");
		numConta = sc.nextLine();
		System.out.println("Informe o nome do correntista:");
		nomeCorrentista = sc.nextLine();

		ContaCorrente contaCorrente = new ContaCorrente(numAgencia, numConta, nomeCorrentista);
		
		ContaCorrenteBO.inserir(contaCorrente);
	}

	public static void encerrar() {

	}

	public static void bloquear() {

	}
}
