package br.ucsal.bes20201.poo.aula14.polimorfismo;

public abstract class Veiculo {

	private String placa;

	private Integer anoFabricacao;

	private Double valor;

	private Pessoa comprador;

	public Veiculo(String placa) {
		super();
		this.placa = placa;
	}

	public abstract Double calcularIPVA(Double desconto);

	public Veiculo(String placa, Integer anoFabricacao, Double valor) {
		super();
		this.placa = placa;
		this.anoFabricacao = anoFabricacao;
		this.valor = valor;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Pessoa getComprador() {
		return comprador;
	}

	public void setComprador(Pessoa comprador) {
		this.comprador = comprador;
	}

	@Override
	public String toString() {
		return "Veiculo [placa=" + placa + ", anoFabricacao=" + anoFabricacao + ", valor=" + valor + ", comprador="
				+ comprador + "]";
	}

}
