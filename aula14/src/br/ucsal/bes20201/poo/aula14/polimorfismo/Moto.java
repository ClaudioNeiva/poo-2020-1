package br.ucsal.bes20201.poo.aula14.polimorfismo;

public class Moto extends Veiculo {

	private CategoriaMotoEnum categoria;

	private Integer qtdCilindradas;

	public Moto(String placa, CategoriaMotoEnum categoria, Integer qtdCilindradas) {
		super(placa);
		this.categoria = categoria;
		this.qtdCilindradas = qtdCilindradas;
	}

	@Override
	public Double calcularIPVA(Double desconto) {
		return 5d * qtdCilindradas - desconto;
	}

	public CategoriaMotoEnum getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaMotoEnum categoria) {
		this.categoria = categoria;
	}

	public Integer getQtdCilindradas() {
		return qtdCilindradas;
	}

	public void setQtdCilindradas(Integer qtdCilindradas) {
		this.qtdCilindradas = qtdCilindradas;
	}

	@Override
	public String toString() {
		return "Moto [categoria=" + categoria + ", qtdCilindradas=" + qtdCilindradas + "]";
	}

}
