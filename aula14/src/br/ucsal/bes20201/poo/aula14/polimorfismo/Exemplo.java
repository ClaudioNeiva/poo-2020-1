package br.ucsal.bes20201.poo.aula14.polimorfismo;

import java.util.ArrayList;
import java.util.List;

public class Exemplo {

	public static void main(String[] args) {

		List<Veiculo> veiculos = new ArrayList<>();

		// UPCast pode implícito ou explícito.
		veiculos.add(new Moto("ABC1234", CategoriaMotoEnum.ESTRADA, 4));
		veiculos.add((Veiculo) new VeiculoPasseio("ERT2134", 2000, 10000.0, 4, 400));
		veiculos.add(new VeiculoCarga("ERT2134", 2010, 50000.0, 4, 8, 300));

		Veiculo veiculo1 = veiculos.get(0);
		// Downcast - explícito!
		Moto moto1 = (Moto) veiculo1;
		System.out.println("veiculo1.ipva = " + moto1.calcularIPVA(.3));

		Double totalIPVA = totalizarIPVA(veiculos);
		System.out.println("totalIPVA(v2)=" + totalIPVA);

	}

	private static Double totalizarIPVA(List<Veiculo> veiculos) {
		Double totalIPVA = 0d;
		for (Veiculo veiculo : veiculos) {
			totalIPVA += veiculo.calcularIPVA(5.);
		}
		return totalIPVA;
	}

}
