package br.ucsal.bes20201.poo.aula14.ordenacao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class OrdenacaoExemplo2 {

	public static void main(String[] args) {
		ordenar();
	}

	private static void ordenar() {
		List<Pessoa> pessoas = new ArrayList<>();
		pessoas.add(new Pessoa(2000, "neiva", "antonio"));
		pessoas.add(new Pessoa(2002, "silva", "claudio"));
		pessoas.add(new Pessoa(2000, "silva", "ana"));
		pessoas.add(new Pessoa(2000, "santos", "clara"));
		pessoas.add(new Pessoa(1995, "neiva", "pedro"));

		System.out.println("Pessoas na ordem que foram informados:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		System.out.println("--------------------------------------");

		Collections.sort(pessoas);
		System.out.println("Pessoas em ordem crescente de ano de nascimento:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		System.out.println("--------------------------------------");

		/*
		 * Essa forma de trabalho, criando uma classe para o comparador não é como
		 * geralmente o mercado trabalha.
		 */
		Collections.sort(pessoas, new ComparadorPessoaSobrenomeNome());
		System.out.println("Pessoas em ordem crescente de sobrenome e nome:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

		System.out.println("--------------------------------------");

		/*
		 * A forma geralmente encontrada é da definição da classe "inline".
		 */
		Collections.sort(pessoas, new Comparator<Pessoa>() {
			@Override
			public int compare(Pessoa pessoa1, Pessoa pessoa2) {
				int resultado = pessoa1.getSobrenome().compareTo(pessoa2.getSobrenome());
				if (resultado == 0) {
					resultado = pessoa1.getNome().compareTo(pessoa2.getNome());
				}
				return resultado;
			}
		});
		System.out.println("Pessoas em ordem crescente de sobrenome e nome:");
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}

	}

}
