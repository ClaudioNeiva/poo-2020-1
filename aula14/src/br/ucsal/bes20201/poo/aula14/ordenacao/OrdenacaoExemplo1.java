package br.ucsal.bes20201.poo.aula14.ordenacao;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class OrdenacaoExemplo1 {

	public static void main(String[] args) {
		ordenar();
	}

	private static void ordenar() {
		List<String> nomes = Arrays.asList("claudio", "antonio", "ana", "pedreira", "neiva");

		System.out.println("Nomes na ordem que foram informados:");
		for (String nome : nomes) {
			System.out.println(nome);
		}

		System.out.println("--------------------------------------");

		Collections.sort(nomes);
		System.out.println("Nomes em ordem alfabética crescente:");
		for (String nome : nomes) {
			System.out.println(nome);
		}

		System.out.println("--------------------------------------");

		/*
		 * O reverse NÃO ordena! Ele simplesmente inverte a ordem dos elementos na
		 * lista. Como a lista estava ordenada (ordem crescente), a reversão fez com que
		 * os nomes ficassem em ordem alfabética decrescente.
		 */
		Collections.reverse(nomes);
		System.out.println("Nomes em ordem alfabética decrescente:");
		for (String nome : nomes) {
			System.out.println(nome);
		}

	}

}
