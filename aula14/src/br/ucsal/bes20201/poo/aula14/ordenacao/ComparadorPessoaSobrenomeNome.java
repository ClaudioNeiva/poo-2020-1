package br.ucsal.bes20201.poo.aula14.ordenacao;

import java.util.Comparator;

/*
 * Essa forma de trabalho, criando uma classe para o comparador não é como geralmente o mercado trabalha.
 */
public class ComparadorPessoaSobrenomeNome implements Comparator<Pessoa> {

	@Override
	public int compare(Pessoa pessoa1, Pessoa pessoa2) {
		int resultado = pessoa1.getSobrenome().compareTo(pessoa2.getSobrenome());
		if (resultado == 0) {
			resultado = pessoa1.getNome().compareTo(pessoa2.getNome());
		}
		return resultado;
	}
}
