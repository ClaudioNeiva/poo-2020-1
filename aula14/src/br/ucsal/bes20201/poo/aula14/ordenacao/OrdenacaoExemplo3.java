package br.ucsal.bes20201.poo.aula14.ordenacao;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class OrdenacaoExemplo3 {

	public static void main(String[] args) {
		ordenar();
	}

	private static void ordenar() {
		List<Pessoa> pessoas = new ArrayList<>();
		pessoas.add(new Pessoa(2000, "neiva", "antonio"));
		pessoas.add(new Pessoa(2002, "silva", "claudio"));
		pessoas.add(new Pessoa(2000, "silva", "ana"));
		pessoas.add(new Pessoa(2000, "santos", "clara"));
		pessoas.add(new Pessoa(1995, "neiva", "pedro"));

		System.out.println("Pessoas na ordem que foram informados:");
		pessoas.stream().forEach(System.out::println);

		System.out.println("--------------------------------------");

		pessoas.sort(Comparator.naturalOrder());
		System.out.println("Pessoas em ordem crescente de ano de nascimento:");
		pessoas.stream().forEach(System.out::println);

		System.out.println("--------------------------------------");

		pessoas.stream().sorted();
		System.out.println("Pessoas em ordem crescente de ano de nascimento:");
		pessoas.stream().forEach(System.out::println);

		System.out.println("--------------------------------------");

		pessoas.stream().sorted(Comparator.comparing(Pessoa::getSobrenome).thenComparing(Pessoa::getNome));
		System.out.println("Pessoas em ordem crescente de sobrenome e nome:");
		pessoas.stream().forEach(System.out::println);

	}

}
