package br.ucsal.bes20201.poo.aula14.ordenacao;

public class Pessoa implements Comparable<Pessoa>{

	private Integer anoNascimento;

	private String sobrenome;

	private String nome;

	public Pessoa(Integer anoNascimento, String sobrenome, String nome) {
		super();
		this.anoNascimento = anoNascimento;
		this.sobrenome = sobrenome;
		this.nome = nome;
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Pessoa [anoNascimento=" + anoNascimento + ", sobrenome=" + sobrenome + ", nome=" + nome + "]";
	}

	@Override
	public int compareTo(Pessoa outraPessoa) {
		return anoNascimento.compareTo(outraPessoa.anoNascimento);
	}

}
