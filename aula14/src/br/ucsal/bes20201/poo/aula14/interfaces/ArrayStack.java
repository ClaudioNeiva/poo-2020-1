package br.ucsal.bes20201.poo.aula14.interfaces;

public class ArrayStack<T> implements Stack<T> {

	private static final int MAX = 100;

	protected Object[] objects = new Object[MAX];

	private int top = 0;

	@Override
	public void push(T e) {
		objects[top] = e;
		top++;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T pop() {
		top--;
		T e = (T) objects[top];
		return e;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T top() {
		return (T) objects[top - 1];
	}

	@Override
	public int size() {
		return top;
	}

}
