package br.ucsal.bes20201.poo.aula14.interfaces;

public class QueueStack<T> implements Stack<T>, Queue<T> {

	/*
	 * Quando um método (mesma assinatura) possui implementação default em mais de
	 * uma interface, você pode escolher a implementação default de uma delas ou
	 * pode criar uma implementação totalmente nova. Neste exemplo, escolhi a
	 * implementação default da interface Queue.
	 */
	@Override
	public int size() {
		return Queue.super.size();
	}

	@Override
	public void push(T e) {
	}

	@Override
	public T pop() {
		return null;
	}

	@Override
	public T top() {
		return null;
	}

	@Override
	public Boolean add(T element) {
		return null;
	}

	@Override
	public T remove() {
		return null;
	}

	@Override
	public T element() {
		return null;
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

}
