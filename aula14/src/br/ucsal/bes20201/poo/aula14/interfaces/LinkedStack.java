package br.ucsal.bes20201.poo.aula14.interfaces;

public class LinkedStack<T> implements Stack<T> {

	private No top;

	private int size = 0;

	@Override
	public void push(T e) {
		No no = new No(e);
		if (top == null) {
			top = no;
		} else {
			no.prox = top;
			top = no;
		}
		size++;
	}

	@Override
	public T pop() {
		T e = top.element;
		top = top.prox;
		size--;
		return e;
	}

	@Override
	public T top() {
		return top.element;
	}

	@Override
	public int size() {
		return size;
	}

	private class No {
		private T element;
		private No prox;

		private No(T element) {
			this.element = element;
		}
	}

}
