package br.ucsal.bes20201.poo.aula14.interfaces;

public class Exemplo1 {

	public static void main(String[] args) {
		executarExemplo();
	}

	private static void executarExemplo() {
		utilizarPilha(new LinkedStack<>());
		System.out.println("------------------");
		utilizarPilha(new ArrayStack<>());
	}

	private static void utilizarPilha(Stack<String> stack1) {
		
		// Late binding
		
		stack1.push("claudio");
		stack1.push("antonio");
		stack1.push("neiva");

		System.out.println("size=" + stack1.size());

		System.out.println("topo da pilha=" + stack1.top());
		System.out.println("topo da pilha=" + stack1.top());
		System.out.println("topo da pilha=" + stack1.top());

		System.out.println("desempilhar=" + stack1.pop());
		System.out.println("desempilhar=" + stack1.pop());
		System.out.println("desempilhar=" + stack1.pop());

		System.out.println("size=" + stack1.size());
	}

}
