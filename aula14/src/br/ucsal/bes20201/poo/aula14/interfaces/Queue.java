package br.ucsal.bes20201.poo.aula14.interfaces;

public interface Queue<T> {

	Boolean add(T element);

	T remove();

	T element();
	
	boolean isEmpty();

	/*
	 * Obs: default implementado para ilustrar o problema da implementação de mais
	 * de uma interface com default para um método de mesma assinatura.
	 */
	default int size() {
		return 10;
	}

}
