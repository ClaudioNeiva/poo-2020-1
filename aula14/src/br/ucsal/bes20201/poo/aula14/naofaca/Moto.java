package br.ucsal.bes20201.poo.aula14.naofaca;

public class Moto extends Veiculo {

	private CategoriaMotoEnum categoria;

	private Integer qtdCilindradas;

	public Moto(String placa, CategoriaMotoEnum categoria, Integer qtdCilindradas) {
		super(placa);
		this.categoria = categoria;
		this.qtdCilindradas = qtdCilindradas;
	}

	public Double calcularIPVA() {
		return 5d * qtdCilindradas;
	}

	public CategoriaMotoEnum getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaMotoEnum categoria) {
		this.categoria = categoria;
	}

	public Integer getQtdCilindradas() {
		return qtdCilindradas;
	}

	public void setQtdCilindradas(Integer qtdCilindradas) {
		this.qtdCilindradas = qtdCilindradas;
	}

}
