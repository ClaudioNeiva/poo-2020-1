package br.ucsal.bes20201.poo.aula13.mapas;

import java.util.Scanner;

public class NumerosQuantidadesArray {

	private static final int QTD = 10;

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		obterNumerosExibirQtd();
	}

	/*
	 * Caso de teste:
	 * 
	 * Entrada: 3, 5, 6, 5, 5, 7, 3, 1, 8, 8
	 * 
	 * Saída esperada:
	 * 
	 * 3 x 2
	 * 
	 * 5 x 3
	 * 
	 * 6 x 1
	 * 
	 * 7 x 1
	 * 
	 * 1 x 1
	 * 
	 * 8 x 2
	 * 
	 * Solução usando vetores
	 * 
	 * Entrada: 3, 5, 6, 5, 5, 7, 3, 1, 8, 8
	 * 
	 * qtdNumerosDistintos = 5
	 *
	 * n = 8
	 * 
	 * pos = 4
	 * 
	 * numerosDistintos = 3 5 6 7 1 8
	 * 
	 * 					  0 1 2 3 4 5 6 7 8 9
	 * 
	 * qtdRepeticoes = 	  2 3 1 1 1 2
	 * 
	 * 					  0 1 2 3 4 5 6 7 8 9
	 * 
	 * 
	 */

	private static void obterNumerosExibirQtd() {
		Integer[] numerosDistintos = new Integer[QTD];
		Integer[] qtdRepeticoes = new Integer[QTD];
		Integer qtdNumerosDistintos;

		qtdNumerosDistintos = obterNumerosCalcularQtd(numerosDistintos, qtdRepeticoes);

		exibirNumerosQtd(numerosDistintos, qtdRepeticoes, qtdNumerosDistintos);
	}

	private static Integer obterNumerosCalcularQtd(Integer[] numerosDistintos, Integer[] qtdRepeticoes) {
		Integer qtdNumerosDistintos = 0;
		Integer n;
		Integer pos;
		System.out.println("Informe " + QTD + " números:");
		for (int i = 0; i < QTD; i++) {
			n = scanner.nextInt();

			pos = obterPosicaoN(numerosDistintos, n, qtdNumerosDistintos);

			if (pos >= 0) {
				qtdRepeticoes[pos]++;
			} else {
				numerosDistintos[qtdNumerosDistintos] = n;
				qtdRepeticoes[qtdNumerosDistintos] = 1;
				qtdNumerosDistintos++;
			}

		}
		return qtdNumerosDistintos;
	}

	/**
	 * Retornar a posição dentro do vetor de distintos na qual se encontra o n.
	 * 
	 * Caso o n não esteja no vetor, este método vai retornar -1.
	 * 
	 * A pesquisa será feita apenas nas posições válidas do vetor, ou seja, posições
	 * cujos valores foram adicionados ao vetor..
	 * 
	 * 
	 * @param numerosDistintos    vetor no qual o n será pesquisado
	 * @param n                   o número que é pesquisado
	 * @param qtdNumerosDistintos é quantidade de itens válidos no vetor
	 * @return a posição no vetor onde n está (se n encontrado) e -1 caso o n não
	 *         esteja no vetor.
	 */
	private static Integer obterPosicaoN(Integer[] numerosDistintos, Integer n, Integer qtdNumerosDistintos) {
		for (int i = 0; i < qtdNumerosDistintos; i++) {
			if (numerosDistintos[i].equals(n)) {
				return i;
			}
		}
		return -1;
	}

	private static void exibirNumerosQtd(Integer[] numerosDistintos, Integer[] qtdRepeticoes,
			Integer qtdNumerosDistintos) {
		for (int i = 0; i < qtdNumerosDistintos; i++) {
			System.out.println(numerosDistintos[i] + " x " + qtdRepeticoes[i]);
		}
	}

}
