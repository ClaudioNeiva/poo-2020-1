package br.ucsal.bes20201.poo.aula13.mapas;

import java.util.HashMap;
import java.util.Map;

public class ExemploMap {

	public static void main(String[] args) {
		preencherPreocurarMap();
	}

	private static void preencherPreocurarMap() {
		// HashMap
		// LinkedHashMap
		// TreeMap
		Map<Integer, String> mapa1 = new HashMap<>();

		// Inserindo dados (keys x values) no mapa;
		mapa1.put(234, "claudio");
		mapa1.put(456, "antonio");
		mapa1.put(432, "pedreira");
		mapa1.put(123, "neiva");
		mapa1.put(789, "ana");
		mapa1.put(123, "paulo");
		mapa1.put(532, "pedreira");

		// Listar o conteúdo do mapa
		for (Integer key : mapa1.keySet()) {
			System.out.println(key + " x " + mapa1.get(key));
		}

		// Perguntar se o mapa contém uma certa chave
		System.out.println("Mapa1 contém a chave 432? " + mapa1.containsKey(432));
		System.out.println("Mapa1 contém a chave 263? " + mapa1.containsKey(263));

		System.out.println("Chave correspondente ao valor ana (primeira ocorrência)= " + keyOf(mapa1, "ana", 0));
		System.out.println("Chave correspondente ao valor ana (segunda ocorrência)= " + keyOf(mapa1, "ana", 1));
		System.out.println(
				"Chave correspondente ao valor pedreira (primeira ocorrência)= " + keyOf(mapa1, "pedreira", 0));
		System.out.println(
				"Chave correspondente ao valor pedreira (saegunda ocorrência)= " + keyOf(mapa1, "pedreira", 1));
		System.out.println("A chave correspondente ao valor clara (primeira ocorrência)= " + keyOf(mapa1, "clara", 0));

	}

	private static Integer keyOf(Map<Integer, String> map, String searchValue, Integer pos) {
		Integer ocorrencia = 0;
		for (Integer key : map.keySet()) {
			if (map.get(key).equals(searchValue)) {
				ocorrencia++;
				if (ocorrencia > pos) {
					return key;
				}
			}
		}
		return null;
	}

}
