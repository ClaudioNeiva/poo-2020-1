package br.ucsal.bes20201.poo.aula13.mapas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class NomesQuantidadesStreamMap {

	public static void main(String[] args) {
		calcularExibirNomesDistintosQtds();
	}

	private static void calcularExibirNomesDistintosQtds() {
		List<String> nomes2 = new ArrayList<>();
		nomes2.add("antonio");
		nomes2.add("claudio");
		nomes2.add("claudio");
		nomes2.add("maria");
		nomes2.add("ana");
		nomes2.add("claudio");
		nomes2.add("maria");
		nomes2.add("joaquim");
		nomes2.add("ana");
		nomes2.add("antonio");

		List<String> nomes1 = Arrays.asList("antonio", "claudio", "claudio", "maria", "ana", "claudio", "maria",
				"joaquim", "ana", "antonio");

		System.out.println("\n********************");
		System.out.println("Nomes:"+nomes1);
		System.out.println("********************\n");
		
		System.out.println("\n********************");
		System.out.println("Nomes:");
		nomes1.stream().forEach(System.out::println);
		System.out.println("********************\n");
		
		// Map< nome , qtd-repetições-do-nome>
		Map<String, Long> nomesDistintosQuantidades = nomes1.stream()
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

		exibirNomesQtd(nomesDistintosQuantidades);
	}

	private static void exibirNomesQtd(Map<String, Long> nomesDistintosQuantidades) {
		for (String nome : nomesDistintosQuantidades.keySet()) {
			System.out.println(nome + " x " + nomesDistintosQuantidades.get(nome));
		}
	}

}
