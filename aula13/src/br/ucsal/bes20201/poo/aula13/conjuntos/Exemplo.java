package br.ucsal.bes20201.poo.aula13.conjuntos;

import java.util.HashSet;
import java.util.Set;

public class Exemplo {

	public static void main(String[] args) {
		carregarAlunosTurma();
	}

	private static void carregarAlunosTurma() {
		Disciplina disciplina1 = new Disciplina("BES008", "Programação Orientada a Objetos", 60);

		Aluno aluno1 = new Aluno(123, "antonio");
		Aluno aluno2 = new Aluno(234, "claudio");
		Aluno aluno3 = new Aluno(345, "pedreira");
		Aluno aluno4 = new Aluno(456, "neiva");

		Aluno aluno5 = new Aluno(123, "antonio");
		Aluno aluno6 = new Aluno(123, "maria");
		Aluno aluno7 = new Aluno(567, "claudio");
		Aluno aluno8 = aluno2;

		Aluno aluno10 = new Aluno(8484, null);
		Aluno aluno11 = null;
		System.out.println("\naluno1.equals(disciplina1)=" + aluno1.equals(disciplina1));
		System.out.println("\naluno1.equals(aluno10)=" + aluno1.equals(aluno10));
		System.out.println("\naluno1.equals(aluno11)=" + aluno1.equals(aluno11));
		System.out.println("\naluno1.equals(aluno5)=" + aluno1.equals(aluno5));
		System.out.println("\naluno1==aluno5=" + (aluno1 == aluno5)+"\n\n");

		// Adicionar os "8 alunos" acima nesta turma
		Set<Aluno> alunos = new HashSet<>();
		alunos.add(aluno1);
		alunos.add(aluno2);
		alunos.add(aluno3);
		alunos.add(aluno4);
		alunos.add(aluno5);
		alunos.add(aluno6);
		alunos.add(aluno7);
		alunos.add(aluno8);

		// Instanciar uma turma
		Turma turma1 = new Turma(123, disciplina1, alunos);

		// Listar os alunos da turma
		System.out.println("Alunos da turma " + turma1.getCodigo() + ":");
		for (Aluno aluno : turma1.getAlunos()) {
			System.out.println(aluno);
		}

	}

}
