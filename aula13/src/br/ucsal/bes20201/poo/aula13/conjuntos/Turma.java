package br.ucsal.bes20201.poo.aula13.conjuntos;

import java.util.HashSet;
import java.util.Set;

public class Turma {

	private Integer codigo;

	private Disciplina disciplina;

	private Set<Aluno> alunos = new HashSet<>();

	public Turma(Integer codigo, Disciplina disciplina, Set<Aluno> alunos) {
		super();
		this.codigo = codigo;
		this.disciplina = disciplina;
		this.alunos = alunos;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Disciplina getDisciplina() {
		return disciplina;
	}

	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}

	public Set<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(Set<Aluno> alunos) {
		this.alunos = alunos;
	}

	@Override
	public String toString() {
		return "Turma [codigo=" + codigo + ", disciplina=" + disciplina + ", alunos=" + alunos + "]";
	}

}
