package br.ucsal.bes20201.poo.aula07.enumeracoes;

public class ContaCorrente {

	private Integer numAgencia;

	private Integer numConta;

	private String nomeCorrentista;

	private Double saldo;

	private SituacaoContaCorrenteEnum situacao;

	public ContaCorrente(Integer numAgencia, Integer numConta, String nomeCorrentista, Double saldo,
			SituacaoContaCorrenteEnum situacao) {
		super();
		this.numAgencia = numAgencia;
		this.numConta = numConta;
		this.nomeCorrentista = nomeCorrentista;
		this.saldo = saldo;
		this.situacao = situacao;
	}

	/*
	 * FIXME Quando aprendermos EXCEPTIONS, o sacar vai ser void e as exceções serão
	 * lançadas quando ocorrerem.
	 */
	public Boolean sacar(Double valor) {
		if (saldo >= valor && situacao.equals(SituacaoContaCorrenteEnum.ATIVA)) {
			saldo -= valor;
			return true;
		}
		return false;
	}

	public Integer getNumAgencia() {
		return numAgencia;
	}

	public void setNumAgencia(Integer numAgencia) {
		this.numAgencia = numAgencia;
	}

	public Integer getNumConta() {
		return numConta;
	}

	public void setNumConta(Integer numConta) {
		this.numConta = numConta;
	}

	public String getNomeCorrentista() {
		return nomeCorrentista;
	}

	public void setNomeCorrentista(String nomeCorrentista) {
		this.nomeCorrentista = nomeCorrentista;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public SituacaoContaCorrenteEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoContaCorrenteEnum situacao) {
		this.situacao = situacao;
	}

}
