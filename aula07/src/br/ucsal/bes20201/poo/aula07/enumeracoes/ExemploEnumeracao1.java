package br.ucsal.bes20201.poo.aula07.enumeracoes;

public class ExemploEnumeracao1 {

	public static void main(String[] args) {
		ContaCorrente contaCorrente1 = new ContaCorrente(1, 20, "claudio", 100., SituacaoContaCorrenteEnum.ATIVA);

		SituacaoContaCorrenteEnum caju = SituacaoContaCorrenteEnum.ATIVA;
		if (SituacaoContaCorrenteEnum.ATIVA.equals(caju)) {
			System.out.println("caju = ATIVA");
		}

		if (caju.equals(SituacaoContaCorrenteEnum.ATIVA)) {
			System.out.println("caju = ATIVA");
		}
		
	}

}
