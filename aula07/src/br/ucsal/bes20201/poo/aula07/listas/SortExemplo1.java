package br.ucsal.bes20201.poo.aula07.listas;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SortExemplo1 {

	static int i = 0;

	public static void main(String[] args) {

		List<String> nomes = new ArrayList<>();

		nomes.add("Claudio Neiva");
		nomes.add("maria da Silva");
		nomes.add("Joaquim Almeida");
		nomes.add("ana");
		nomes.add("clara");

		System.out.println("Ordem naturalOrder:");
		nomes.sort(Comparator.naturalOrder());
		nomes.forEach(nome -> System.out.println(nome));

		System.out.println("Ordem naturalOrder (mesmo comportamento, comandos diferentes):");
		for (String n : nomes) {
			System.out.println(n);
		}

		System.out.println("\n\nOrdem case insensitive:");
		nomes.sort(String.CASE_INSENSITIVE_ORDER);
		nomes.forEach(n -> System.out.println(n));

		List<Integer> notas = new ArrayList<>();

		notas.add(5);
		notas.add(3);
		notas.add(4);

		System.out.println("\n\nOrdenar em ordem crescente:");
		notas.sort(Comparator.naturalOrder());
		notas.forEach(n -> System.out.println(n));

		System.out.println("\nOrdenar em ordem decrescente:");
		notas.stream().sorted(Comparator.reverseOrder()).forEach(n -> System.out.println(n));

	}
}
