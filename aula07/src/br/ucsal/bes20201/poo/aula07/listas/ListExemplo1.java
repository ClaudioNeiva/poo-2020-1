package br.ucsal.bes20201.poo.aula07.listas;

import java.util.ArrayList;
import java.util.List;

public class ListExemplo1 {

	public static void main(String[] args) {

		List<String> nomes = new ArrayList<>();
		nomes.add("claudio");
		// nomes.add(10);

		List<Aluno> alunos = new ArrayList<>();
		alunos.add(new Aluno(123, "claudio neiva", "claudio@ucsal.br"));

		/* Não é recomendável criar uma lista sem definir o tipo de dado que a mesma */
		List coisas = new ArrayList();
		coisas.add(123123);
		coisas.add("claudio");
		coisas.add(new Aluno(123, "claudio neiva", "claudio@ucsal.br"));

		List<Object> objetos = new ArrayList<>();
		objetos.add(123123);
		objetos.add("claudio");
		objetos.add(new Aluno(123, "claudio neiva", "claudio@ucsal.br"));
	}

}
