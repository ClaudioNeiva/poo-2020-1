package br.ucsal.bes20201.poo.aula11.mapas;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class NumerosQuantidadesMap {

	/*
	 * Map: An object that maps keys to values. A map cannot contain duplicate keys;
	 * each key can map to at most one value.
	 */

	/*
	 * Caso de teste:
	 * 
	 * Entrada: 3, 5, 6, 5, 5, 7, 3, 1, 8, 8
	 * 
	 * Saída esperada:
	 * 
	 * 3 x 2
	 * 
	 * 5 x 3
	 * 
	 * 6 x 1
	 * 
	 * 7 x 1
	 * 
	 * 1 x 1
	 * 
	 * 8 x 2
	 * 
	 */

	private static final int QTD = 10;

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		obterNumerosExibirQtd();
	}

	private static void obterNumerosExibirQtd() {
		// Map< número , qtd-repetições-do-numero>
		Map<Integer, Integer> numerosDistintosQuantidades = obterNumerosCalcularQtd();
		
		exibirNumerosQtd(numerosDistintosQuantidades);
	}

	private static Map<Integer, Integer> obterNumerosCalcularQtd() {
		Map<Integer, Integer> numerosDistintosQuantidades = new HashMap<>();
		Integer n;
		System.out.println("Informe " + QTD + " números:");
		for (int i = 0; i < QTD; i++) {
			n = scanner.nextInt();

			if (numerosDistintosQuantidades.containsKey(n)) {
				Integer qtdAtual = numerosDistintosQuantidades.get(n);
				numerosDistintosQuantidades.put(n, qtdAtual + 1);
			} else {
				numerosDistintosQuantidades.put(n, 1);
			}

		}
		return numerosDistintosQuantidades;
	}

	private static void exibirNumerosQtd(Map<Integer, Integer> numerosDistintosQuantidades) {
		for (Integer n : numerosDistintosQuantidades.keySet()) {
			System.out.println(n + " x " + numerosDistintosQuantidades.get(n));
		}
	}

}
