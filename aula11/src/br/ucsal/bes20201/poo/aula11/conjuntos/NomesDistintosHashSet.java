package br.ucsal.bes20201.poo.aula11.conjuntos;

import java.util.HashSet;
import java.util.Set;

public class NomesDistintosHashSet extends NomesDistintosSet {

	public static void main(String[] args) {
		NomesDistintosSet nomesDistintosSet = new NomesDistintosHashSet();
		nomesDistintosSet.obterExibirNomesDistintos();

		// new NomesDistintosHashSet().obterExibirNomesDistintos();
	}

	@Override
	protected Set<String> obterImplementacaoSet() {
		return new HashSet<>();
	}

}
