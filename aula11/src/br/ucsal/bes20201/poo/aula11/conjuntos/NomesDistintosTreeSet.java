package br.ucsal.bes20201.poo.aula11.conjuntos;

import java.util.Set;
import java.util.TreeSet;

public class NomesDistintosTreeSet extends NomesDistintosSet {

	public static void main(String[] args) {
		NomesDistintosSet nomesDistintosSet = new NomesDistintosTreeSet();
		nomesDistintosSet.obterExibirNomesDistintos();

		// new NomesDistintosHashSet().obterExibirNomesDistintos();
	}

	@Override
	protected Set<String> obterImplementacaoSet() {
		return new TreeSet<String>();
	}

}
