package br.ucsal.bes20201.poo.aula11.conjuntos;

import java.util.LinkedHashSet;
import java.util.Set;

public class NomesDistintosLinkedHashSet extends NomesDistintosSet {

	public static void main(String[] args) {
		NomesDistintosSet nomesDistintosSet = new NomesDistintosLinkedHashSet();
		nomesDistintosSet.obterExibirNomesDistintos();

		// new NomesDistintosHashSet().obterExibirNomesDistintos();
	}

	@Override
	protected Set<String> obterImplementacaoSet() {
		return new LinkedHashSet<String>();
	}

}
