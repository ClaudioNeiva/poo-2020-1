package br.ucsal.bes20201.poo.aula11.conjuntos;

import java.util.Scanner;
import java.util.Set;

public abstract class NomesDistintosSet {

	private static final int QTD_NOMES = 4;

	private static Scanner scanner = new Scanner(System.in);

	/*
	 * Set:
	 * 
	 * A collection that contains no duplicate elements;
	 * 
	 * More formally, sets contain no pair of elements e1 and e2 such that
	 * e1.equals(e2), and at most one null element.
	 * 
	 * 
	 * HashSet:
	 * 
	 * This class implements the Set interface, backed by a hash table (actually a
	 * HashMap instance). It makes no guarantees as to the iteration order of the
	 * set; in particular, it does not guarantee that the order will remain constant
	 * over time. This class permits the null element.
	 * 
	 * 
	 * TreeSet:
	 * 
	 * A NavigableSet implementation based on a TreeMap. The elements are ordered
	 * using their natural ordering, or by a Comparator provided at set creation
	 * time, depending on which constructor is used.
	 * 
	 * 
	 * LinkedHashSet:
	 * 
	 * Hash table and linked list implementation of the Set interface, with
	 * predictable iteration order. This implementation differs from HashSet in that
	 * it maintains a doubly-linked list running through all of its entries. This
	 * linked list defines the iteration ordering, which is the order in which
	 * elements were inserted into the set (insertion-order). Note that insertion
	 * order is not affected if an element is re-inserted into the set.
	 * 
	 */

	protected void obterExibirNomesDistintos() {
		Set<String> nomes = obterNomesDistintos(QTD_NOMES);
		exibirNomes(nomes);
	}

	private Set<String> obterNomesDistintos(int qtd) {
		Set<String> nomes = obterImplementacaoSet();
		String nome;
		System.out.println("Informe " + qtd + " nomes distintos:");
		while (nomes.size() < qtd) {
			nome = scanner.nextLine();
			nomes.add(nome);
		}
		return nomes;
	}

	// Estudar polimorfismo!!!
	protected abstract Set<String> obterImplementacaoSet();

	private void exibirNomes(Set<String> nomes) {
		System.out.println("Nomes informados: ");
		for (String nome : nomes) {
			System.out.println(nome);
		}
	}

}
