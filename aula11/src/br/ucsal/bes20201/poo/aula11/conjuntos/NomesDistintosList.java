package br.ucsal.bes20201.poo.aula11.conjuntos;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class NomesDistintosList {

	private static final int QTD_NOMES = 4;

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		obterExibirNomesDistintos();
	}

	private static void obterExibirNomesDistintos() {
		List<String> nomes = obterNomesDistintos(QTD_NOMES);
		exibirNomes(nomes);
	}

	private static List<String> obterNomesDistintos(int qtd) {
		List<String> nomes = new ArrayList<>();
		String nome;
		System.out.println("Informe " + qtd + " nomes distintos:");
		while (nomes.size() < qtd) {
			nome = scanner.nextLine();
			if (!nomes.contains(nome)) {
				nomes.add(nome);
			}
		}
		return nomes;
	}

	private static void exibirNomes(List<String> nomes) {
		System.out.println("Nomes informados: ");
		for (String nome : nomes) {
			System.out.println(nome);
		}

	}

}
