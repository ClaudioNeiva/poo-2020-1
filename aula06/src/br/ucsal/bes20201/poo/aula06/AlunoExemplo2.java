package br.ucsal.bes20201.poo.aula06;

public class AlunoExemplo2 {

	public static void main(String[] args) {

		Aluno aluno1 = new Aluno("claudio neiva", "claudio@ucsal.br");

		System.out.println(aluno1.getClass().getCanonicalName() + "@" +  Integer.toHexString(aluno1.hashCode()) + "|aluno1=" + aluno1);

		System.out.println(aluno1.toString());

	}

}
