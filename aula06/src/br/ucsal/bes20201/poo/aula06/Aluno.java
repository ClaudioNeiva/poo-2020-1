package br.ucsal.bes20201.poo.aula06;

public class Aluno {

	private static Integer seq = 0;

	private Integer matricula;

	private String nome;

	private String email;

	public Aluno(String nome, String email) {
		this.nome = nome;
		this.email = email;

		seq++;
		matricula = seq;
	}

	public static Integer getSeq() {
		return seq;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getMatricula() {
		return matricula;
	}

	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", nome=" + nome + ", email=" + email + "]";
	}

}
