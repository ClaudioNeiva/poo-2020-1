package br.ucsal.bes20201.poo.aula06;

import java.util.ArrayList;
import java.util.List;

public class ListExemplo4 {

	public static void main(String[] args) {
		
		List<Object> objetosDiversos = new ArrayList<>();
		objetosDiversos.add("Claudio Neiva");
		objetosDiversos.add(1234123);
		objetosDiversos.add(new Aluno("claudio", "claudio@ucsal.br"));

		 List<String> nomes = new ArrayList<>();
		//List<String> nomes = new LinkedList<>();

		nomes.add("Claudio Neiva");
		nomes.add("Maria da Silva");
		nomes.add("Joaquim Almeida");

		System.out.println("nomes=" + nomes);

		System.out.println("tamanho da lista=" + nomes.size());

		System.out.println("Qual o índice de Joaquim Almeida na lista? " + nomes.indexOf("Joaquim Almeida"));

		System.out.println("Em qual posição da lista está Joaquim Almeida? " + (nomes.indexOf("Joaquim Almeida") + 1));

		System.out.println("Primeiro nome=" + nomes.get(0));

		nomes.remove(1);

		System.out.println("lista nomes está vazia? " + nomes.isEmpty());

		System.out.println("nomes=" + nomes);

		System.out.println("tamanho da lista=" + nomes.size());

		System.out.println("A lista de nomes contem o nome Joaquim Almeida? " + nomes.contains("Joaquim Almeida"));

		System.out.println("A lista de nomes contem o nome Joaquim Almeida? " + nomes.contains("Maria da Silva"));

		System.out.println("Qual o índice de Joaquim Almeida na lista? " + nomes.indexOf("Joaquim Almeida"));

		System.out.println("Em qual posição da lista está Joaquim Almeida? " + (nomes.indexOf("Joaquim Almeida") + 1));
	}

}
