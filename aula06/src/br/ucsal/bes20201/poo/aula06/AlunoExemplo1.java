package br.ucsal.bes20201.poo.aula06;

public class AlunoExemplo1 {

	public static void main(String[] args) {
		
		System.out.println("Aluno.seq=" + Aluno.getSeq());

		Aluno aluno1 = new Aluno("claudio neiva", "claudio@ucsal.br");
		Aluno aluno2 = new Aluno("maria da silva", "maria@ucsal.br");
		Aluno aluno3 = new Aluno("pedro de almeida", "pedro@ucsal.br");

		System.out.println(aluno1 + "|aluno1|nome = " + aluno1.getNome() + "|matricula = " + aluno1.getMatricula());
		System.out.println(aluno2 + "|aluno2|nome = " + aluno2.getNome() + "|matricula = " + aluno2.getMatricula());
		System.out.println(aluno3 + "|aluno3|nome = " + aluno3.getNome() + "|matricula = " + aluno3.getMatricula());

		System.out.println("Aluno.seq=" + Aluno.getSeq());

		// aluno1.matricula = 1
		// aluno2.matricula = 2
		// aluno3.matricula = 3
	}

}
