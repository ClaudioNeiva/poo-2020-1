package br.ucsal.bes20201.poo.aula06;

import java.util.Scanner;

public class AlunoExemplo3 {

	private static final int QTD_MAX_ALUNOS = 10;
	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		utilizarVetoresArmazenarAlunos2();
	}

	private static void utilizarVetoresArmazenarAlunos2() {
		Aluno[] alunos = new Aluno[QTD_MAX_ALUNOS];
		String nome;
		String email;
		String resposta;
		int indice;
		int qtd = 0;
		do {
			System.out.println("Dados do aluno " + (qtd + 1));
			System.out.println("\tNome:");
			nome = scanner.nextLine();
			System.out.println("\tEmail:");
			email = scanner.nextLine();
			alunos[qtd] = new Aluno(nome, email);
			qtd++;
			System.out.println("Deseja informar outro aluno (S/N)?");
			resposta = scanner.nextLine();
		} while (qtd < QTD_MAX_ALUNOS && resposta.equalsIgnoreCase("S"));

		System.out.println("Informe o índice (0 a " + QTD_MAX_ALUNOS + ") do aluno a ser removido da lista:");
		indice = scanner.nextInt();
		scanner.nextLine();
		// Como remover um item do meu vetor?
		for (int i = indice; i < qtd - 1; i++) {
			alunos[i] = alunos[i + 1];
		}
		alunos[qtd - 1] = null;
		// Quantos elementos estão atualmente no meu vetor?
		System.out.println("qtd alunos = " + qtd);
	}

	public static void utilizarVetoresArmazenarAlunos() {
		Aluno[] alunos = new Aluno[QTD_MAX_ALUNOS];

		alunos[0] = new Aluno("claudio neiva", "claudio@ucsal.br");
		alunos[1] = new Aluno("maria da silva", "maria@ucsal.br");
		alunos[2] = new Aluno("pedro de almeida", "pedro@ucsal.br");

		System.out.println(alunos[0]);
		System.out.println(alunos[1]);
		System.out.println(alunos[2]);
	}

}
