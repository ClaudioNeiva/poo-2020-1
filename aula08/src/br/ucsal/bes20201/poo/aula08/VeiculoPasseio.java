package br.ucsal.bes20201.poo.aula08;

public class VeiculoPasseio extends Veiculo {

	private Integer qtdPassageiras;

	private Integer capacidadePortaMalas;

	public VeiculoPasseio(String placa, Integer anoFabricacao, Double valor, Integer qtdPassageiras,
			Integer capacidadePortaMalas) {
		super(placa, anoFabricacao, valor);
		this.qtdPassageiras = qtdPassageiras;
		this.capacidadePortaMalas = capacidadePortaMalas;
	}

	public Integer getQtdPassageiras() {
		return qtdPassageiras;
	}

	public void setQtdPassageiras(Integer qtdPassageiras) {
		this.qtdPassageiras = qtdPassageiras;
	}

	public Integer getCapacidadePortaMalas() {
		return capacidadePortaMalas;
	}

	public void setCapacidadePortaMalas(Integer capacidadePortaMalas) {
		this.capacidadePortaMalas = capacidadePortaMalas;
	}
}
