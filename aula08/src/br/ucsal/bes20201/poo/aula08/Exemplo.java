package br.ucsal.bes20201.poo.aula08;

import java.util.ArrayList;
import java.util.List;

public class Exemplo {

	public static void main(String[] args) {

		List<Veiculo> veiculos = new ArrayList<>();

		veiculos.add(new Moto("ABC1234", CategoriaMotoEnum.ESTRADA, 4));
		veiculos.add(new VeiculoPasseio("ERT2134", 2000, 10000.0, 4, 400));
		veiculos.add(new VeiculoCarga("ERT2134", 2000, 10000.0, 4, 8, 20));

		System.out.println(veiculos.get(0).getPlaca());
		System.out.println(veiculos.get(0).getValor());
		System.out.println(veiculos.get(0).getAnoFabricacao());

		System.out.println(((Moto) veiculos.get(0)).getCategoria());
		System.out.println(((VeiculoPasseio) veiculos.get(1)).getQtdPassageiras());

		// Upcast
		Veiculo veiculo = new Moto("ASD1234", CategoriaMotoEnum.ESTRADA, 4);

		System.out.println(veiculo.getPlaca());

		// Downcast
		Moto moto = (Moto) veiculo;
		System.out.println(moto.getCategoria());

//		Moto moto1 = new Moto();
//		moto1.setCategoria(CategoriaMotoEnum.ESTRADA);
//		moto1.setPlaca("ABC1234");
//
//		Pessoa pessoa1 = new Pessoa();
//		pessoa1.cpf = "123123";
//		pessoa1.nome = "Claudio";
//		pessoa1.telefone = "7198998998";
//
//		moto1.comprador = pessoa1;
//
//		// Solução 1
//		Endereco endereco = new Endereco();
//		endereco.cep = "40000000";
//		endereco.logradouro = "Rua x";
//		endereco.numero = "s/n";
//		endereco.bairro = "Pituaçu";
//		pessoa1.endereco = endereco;
//
//		// Solução 2
//		pessoa1.endereco = new Endereco();
//		pessoa1.endereco.cep = "40000000";
//		pessoa1.endereco.logradouro = "Rua x";
//		pessoa1.endereco.numero = "s/n";
//		pessoa1.endereco.bairro = "Pituaçu";
//
//		// Solução 1 e solução 2 produzem o mesmo resultado.

	}

}
