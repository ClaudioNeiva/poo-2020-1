package br.ucsal.bes20201.poo.aula08;

public class VeiculoCarga extends Veiculo {

	private Integer capacidadeCarga;

	private Integer qtdEixos;

	private Integer capacidadeTanqueCombustivel;

	public VeiculoCarga(String placa, Integer anoFabricacao, Double valor, Integer capacidadeCarga, Integer qtdEixos,
			Integer capacidadeTanqueCombustivel) {
		super(placa, anoFabricacao, valor);
		this.capacidadeCarga = capacidadeCarga;
		this.qtdEixos = qtdEixos;
		this.capacidadeTanqueCombustivel = capacidadeTanqueCombustivel;
	}

	public Integer getCapacidadeCarga() {
		return capacidadeCarga;
	}

	public void setCapacidadeCarga(Integer capacidadeCarga) {
		this.capacidadeCarga = capacidadeCarga;
	}

	public Integer getQtdEixos() {
		return qtdEixos;
	}

	public void setQtdEixos(Integer qtdEixos) {
		this.qtdEixos = qtdEixos;
	}

	public Integer getCapacidadeTanqueCombustivel() {
		return capacidadeTanqueCombustivel;
	}

	public void setCapacidadeTanqueCombustivel(Integer capacidadeTanqueCombustivel) {
		this.capacidadeTanqueCombustivel = capacidadeTanqueCombustivel;
	}

}
