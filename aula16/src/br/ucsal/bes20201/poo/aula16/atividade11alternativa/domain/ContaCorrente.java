package br.ucsal.bes20201.poo.aula16.atividade11alternativa.domain;

import br.ucsal.bes20201.poo.aula16.atividade11alternativa.exception.NegocioException;;

public class ContaCorrente {

	private static final double SALDO_INICIAL = 0d;

	private String numAgencia;

	private String numConta;

	private String nomeCorrentista;

	private Double saldo;

	public ContaCorrente(String numAgencia, String numConta, String nomeCorrentista) {
		this.numAgencia = numAgencia;
		this.numConta = numConta;
		this.nomeCorrentista = nomeCorrentista;
		saldo = SALDO_INICIAL;
	}

	public void sacar(Double valor) throws NegocioException {
		if (saldo < valor) {
			throw new NegocioException("Saldo insuficiente. Saldo disponível = " + saldo);
		}
		saldo -= valor;
	}

	public String getNumAgencia() {
		return numAgencia;
	}

	public String getNumConta() {
		return numConta;
	}

	public String getNomeCorrentista() {
		return nomeCorrentista;
	}

	public void setNomeCorrentista(String nomeCorrentista) {
		/*
		 * Posso escreve aqui regras de negócio que validam o nomeCorrentista antes de
		 * atualizar o atributo.
		 */
		this.nomeCorrentista = nomeCorrentista;
	}
}
