package br.ucsal.bes20201.poo.aula16.atividade11alternativa.exception;

public class ContaCorrenteJaCadastradaException extends NegocioException {

	private static final long serialVersionUID = 1L;

	public ContaCorrenteJaCadastradaException(String message) {
		super(message);
	}

}
