package br.ucsal.bes20201.poo.aula16.atividade11alternativa.exception;

public class NegocioException extends Exception {

	private static final long serialVersionUID = 1L;

	public NegocioException(String message) {
		super(message);
	}

}
