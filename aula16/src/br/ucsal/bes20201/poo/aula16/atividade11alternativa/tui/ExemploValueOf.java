package br.ucsal.bes20201.poo.aula16.atividade11alternativa.tui;

import java.util.Scanner;

public class ExemploValueOf {

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		ilustrarUsoValueOf();
		ilustrarUsoValueOfCodigo();
	}

	private static void ilustrarUsoValueOfCodigo() {
		System.out.println("Informe o código item de menu:");
		Integer codigoMenuItem = sc.nextInt();
		MenuEnum menuItem = MenuEnum.valueOfCodigo(codigoMenuItem);
		System.out.println("O item informado foi: " + menuItem.obterDescricaoDetalhada());
	}

	private static void ilustrarUsoValueOf() {
		System.out.println("Informe o item de menu:");
		String menuItemString = sc.nextLine();
		MenuEnum menuItem = MenuEnum.valueOf(menuItemString);
		System.out.println("O item informado foi: " + menuItem.obterDescricaoDetalhada());
	}

}
