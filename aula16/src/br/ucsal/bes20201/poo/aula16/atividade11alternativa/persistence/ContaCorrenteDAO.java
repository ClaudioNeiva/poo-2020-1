package br.ucsal.bes20201.poo.aula16.atividade11alternativa.persistence;

import br.ucsal.bes20201.poo.aula16.atividade11alternativa.domain.ContaCorrente;
import br.ucsal.bes20201.poo.aula16.atividade11alternativa.exception.NegocioException;

public class ContaCorrenteDAO {

	private static final int QTD_MAX = 1000;

	private static ContaCorrente[] contasCorrente = new ContaCorrente[QTD_MAX];
	private static int qtd = 0;

	public static void inserir(ContaCorrente contaCorrente) {
		contasCorrente[qtd] = contaCorrente;
		qtd++;
	}

	public static ContaCorrente obter(String numAgencia, String numConta) throws NegocioException  {
		for (int i = 0; i < qtd; i++) {
			if (numAgencia.equals(contasCorrente[i].getNumAgencia())
					&& numConta.equals(contasCorrente[i].getNumConta())) {
				return contasCorrente[i];
			}
		}
		/*
		 * Quando eu chego neste ponto do código, significa que não foi encontrada
		 * nenhum conta corrente para a numAgencia e numConta passados como parâmetro.
		 */
		throw new NegocioException("ContaCorrente não encontrada para agência " + numAgencia + " conta " + numConta);
	}

}
