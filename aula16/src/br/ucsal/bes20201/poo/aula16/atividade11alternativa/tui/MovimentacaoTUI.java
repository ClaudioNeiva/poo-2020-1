package br.ucsal.bes20201.poo.aula16.atividade11alternativa.tui;

import java.util.Scanner;

import br.ucsal.bes20201.poo.aula16.atividade11alternativa.business.ContaCorrenteBO;
import br.ucsal.bes20201.poo.aula16.atividade11alternativa.domain.ContaCorrente;
import br.ucsal.bes20201.poo.aula16.atividade11alternativa.exception.NegocioException;

public class MovimentacaoTUI {

	private static Scanner sc = new Scanner(System.in);

	public static void sacar() {
		String numAgencia;
		String numConta;
		Double valor;

		System.out.println("Informe o número da agência:");
		numAgencia = sc.nextLine();
		System.out.println("Informe o número da conta:");
		numConta = sc.nextLine();
		System.out.println("Informe o valor:");
		valor = sc.nextDouble();
		sc.nextLine();

		ContaCorrente contaCorrente;
		try {
			contaCorrente = ContaCorrenteBO.obter(numAgencia, numConta);
			contaCorrente.sacar(valor);
			System.out.println("Saque realizado. com sucesso na conta de " + contaCorrente.getNomeCorrentista());
		} catch (NegocioException e) {
			System.out.println(e.getMessage());
		}

	}

	public static void depositar() {

	}

	public static void consultarSaldo() {

	}

	static void emitirExtrato() {

	}
}
