package br.ucsal.bes20201.poo.aula16.atividade11.exception;

public class RegistroNaoEncontradoException extends Exception {

	private static final long serialVersionUID = 1L;

	public RegistroNaoEncontradoException(String message) {
		super(message);
	}

}
