package br.ucsal.bes20201.poo.aula16.atividade11.tui;

import java.util.Scanner;

import br.ucsal.bes20201.poo.aula16.atividade11.business.ContaCorrenteBO;
import br.ucsal.bes20201.poo.aula16.atividade11.domain.ContaCorrente;
import br.ucsal.bes20201.poo.aula16.atividade11.exception.RegistroNaoEncontradoException;
import br.ucsal.bes20201.poo.aula16.atividade11.exception.SaldoInsuficienteException;

public class MovimentacaoTUI {

	private static Scanner sc = new Scanner(System.in);

	public static void sacar() {
		String numAgencia;
		String numConta;
		Double valor;

		System.out.println("Informe o número da agência:");
		numAgencia = sc.nextLine();
		System.out.println("Informe o número da conta:");
		numConta = sc.nextLine();
		System.out.println("Informe o valor:");
		valor = sc.nextDouble();
		sc.nextLine();

		ContaCorrente contaCorrente;
		try {
			contaCorrente = ContaCorrenteBO.obter(numAgencia, numConta);
			contaCorrente.sacar(valor);
			System.out.println("Saque realizado. com sucesso na conta de " + contaCorrente.getNomeCorrentista());
			// } catch (RegistroNaoEncontradoException | SaldoInsuficienteException e) {
			// System.out.println(e.getMessage());
		} catch (RegistroNaoEncontradoException e) {
			System.out.println(e.getMessage());
		} catch (SaldoInsuficienteException e) {
			System.out.println(e.getMessage());
			System.out.println("Contrate com seu gerente um limite de crédito!");
		}

	}

	public static void depositar() {

	}

	public static void consultarSaldo() {

	}

	static void emitirExtrato() {

	}
}
