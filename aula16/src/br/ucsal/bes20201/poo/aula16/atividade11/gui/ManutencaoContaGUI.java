package br.ucsal.bes20201.poo.aula16.atividade11.gui;

import br.ucsal.bes20201.poo.aula16.atividade11.business.ContaCorrenteBO;
import br.ucsal.bes20201.poo.aula16.atividade11.domain.ContaCorrente;
import br.ucsal.bes20201.poo.aula16.atividade11.exception.ContaCorrenteJaCadastradaException;

public class ManutencaoContaGUI {

	public static void abrir() {
		String numAgencia = "";
		String numConta = "";
		String nomeCorrentista = "";

		// Aqui estarão os comandos Java Swing de entrada e saída.

		ContaCorrente contaCorrente = new ContaCorrente(numAgencia, numConta, nomeCorrentista);

		try {
			ContaCorrenteBO.inserir(contaCorrente);
		} catch (ContaCorrenteJaCadastradaException e) {
			// Aqui estarão os comandos Java Swing para exibição da mensagem.
		}

	}
}
