package br.ucsal.bes20201.poo.aula16.atividade11.tui;

import java.util.Scanner;

import br.ucsal.bes20201.poo.aula16.atividade11.business.ContaCorrenteBO;
import br.ucsal.bes20201.poo.aula16.atividade11.domain.ContaCorrente;
import br.ucsal.bes20201.poo.aula16.atividade11.exception.ContaCorrenteJaCadastradaException;

public class ManutencaoContaTUI {

	private static Scanner sc = new Scanner(System.in);

	public static void abrir() {
		String numAgencia;
		String numConta;
		String nomeCorrentista;

		System.out.println("Informe o número da agência:");
		numAgencia = sc.nextLine();
		System.out.println("Informe o número da conta:");
		numConta = sc.nextLine();
		System.out.println("Informe o nome do correntista:");
		nomeCorrentista = sc.nextLine();

		ContaCorrente contaCorrente = new ContaCorrente(numAgencia, numConta, nomeCorrentista);

		try {
			ContaCorrenteBO.inserir(contaCorrente);
			System.out.println("Conta corrente cadastada com sucesso.");
		} catch (ContaCorrenteJaCadastradaException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void encerrar() {

	}

	public static void bloquear() {

	}
}
