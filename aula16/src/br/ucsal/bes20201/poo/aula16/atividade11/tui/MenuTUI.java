package br.ucsal.bes20201.poo.aula16.atividade11.tui;

import java.util.Scanner;

public class MenuTUI {

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		executarMenu();
	}

	private static void executarMenu() {
		MenuEnum opcao;
		do {
			exibirOpcoes();
			opcao = selecionarOpcao();
			executarOpcao(opcao);
		} while (!opcao.equals(MenuEnum.SAIR));
	}

	private static void exibirOpcoes() {
		for (MenuEnum menu : MenuEnum.values()) {
			System.out.println(menu.obterDescricaoDetalhada());
		}
	}

	private static void executarOpcao(MenuEnum opcao) {
		switch (opcao) {
		case ABERTURA_CONTA:
			ManutencaoContaTUI.abrir();
			break;
		case SAQUE:
			MovimentacaoTUI.sacar();
			break;
		case DEPOSITO:
			System.out.println("Opção não implementada!");
			break;
		case SAIR:
			System.out.println("Bye...");
			break;
		default:
			break;
		}
	}

	private static MenuEnum selecionarOpcao() {
		MenuEnum opcao;
		Integer opcaoInteger;
		do {
			System.out.println("Opção:");
			opcaoInteger = sc.nextInt();
			sc.nextLine();
			opcao = MenuEnum.valueOfCodigo(opcaoInteger);
			if(opcao == null) {
				System.out.println("Opção não válida!");
			}
		} while (opcao == null);
		return opcao;
	}

}
