package br.ucsal.bes20201.poo.aula16.atividade11.exception;

public class SaldoInsuficienteException extends Exception {

	private static final long serialVersionUID = 1L;

	private static final String DEFAULT_MESSAGE = "Saldo insuficiente. Saldo disponível ";

	public SaldoInsuficienteException(Double saldoAtual) {
		super(DEFAULT_MESSAGE + saldoAtual);
	}

}
