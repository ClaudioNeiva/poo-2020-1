package br.ucsal.bes20201.poo.aula16.atividade11.persistence;

import br.ucsal.bes20201.poo.aula16.atividade11.domain.ContaCorrente;
import br.ucsal.bes20201.poo.aula16.atividade11.exception.RegistroNaoEncontradoException;

public class ContaCorrenteDAO {

	private static final int QTD_MAX = 1000;

	private static ContaCorrente[] contasCorrente = new ContaCorrente[QTD_MAX];
	private static int qtd = 0;

	public static void inserir(ContaCorrente contaCorrente) {
		/*
		 * O DAO não valida, por exemplo, se campos obrigatórios foram informados! O DAO
		 * não faz alteração em atributos, não faz contas, "processamento" (tranformação
		 * de dados). O DAO simplesmente armazena/remove/substitui ou retorna objetos.
		 */

		contasCorrente[qtd] = contaCorrente;
		qtd++;
	}

	public static void remover(ContaCorrente contaCorrente) {
		/*
		 * Implementação para remoção da contaCorrente passada como parâmetro do vetor
		 * contasCorrente.
		 */
	}

	public static void atualizar(ContaCorrente contaCorrente) {
		/*
		 * Implementação para substituir a contaCorrente passada como parâmetro no vetor
		 * contasCorrente.
		 */
	}

	public static ContaCorrente[] obterTodos() {
		return contasCorrente;
	}

	public static ContaCorrente obter(String nomeCorrentista) {
		/*
		 * Implementação da pesquisa por nome do correntista.
		 */
		return null;
	}

	public static ContaCorrente obter(String numAgencia, String numConta) throws RegistroNaoEncontradoException {
		for (int i = 0; i < qtd; i++) {
			if (numAgencia.equals(contasCorrente[i].getNumAgencia())
					&& numConta.equals(contasCorrente[i].getNumConta())) {
				return contasCorrente[i];
			}
		}
		/*
		 * Quando eu chego neste ponto do código, significa que não foi encontrada
		 * nenhum conta corrente para a numAgencia e numConta passados como parâmetro.
		 */
		throw new RegistroNaoEncontradoException(
				"ContaCorrente não encontrada para agência " + numAgencia + " conta " + numConta);
	}

}
