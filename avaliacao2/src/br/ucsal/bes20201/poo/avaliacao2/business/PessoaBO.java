package br.ucsal.bes20201.poo.avaliacao2.business;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import br.ucsal.bes20201.poo.avaliacao2.domain.Aluno;
import br.ucsal.bes20201.poo.avaliacao2.domain.Pessoa;
import br.ucsal.bes20201.poo.avaliacao2.domain.Professor;
import br.ucsal.bes20201.poo.avaliacao2.domain.TitulacaoEnum;
import br.ucsal.bes20201.poo.avaliacao2.exception.ValorNaoInformadoException;
import br.ucsal.bes20201.poo.avaliacao2.persistence.PessoaDAO;

public class PessoaBO {

	public static void cadastrarProfessor(String nome, String telefone, String nacionalidade, TitulacaoEnum titulacao) {
		Professor professor = new Professor(nome, telefone, nacionalidade, titulacao);
		PessoaDAO.inserir(professor);
	}

	public static void cadastrarAluno(String nome, String telefone, String nacionalidade, Integer anoNascimento,
			String escolaOrigem) throws ValorNaoInformadoException {
		Aluno aluno = new Aluno(nome, telefone, nacionalidade, anoNascimento, escolaOrigem);
		PessoaDAO.inserir(aluno);
	}

	public static Set<String> obterNomesEscolaOrigem() {
		Set<String> nomesEscolasOrigem = new TreeSet<>();
		for (Aluno aluno : PessoaDAO.obterAlunos()) {
			nomesEscolasOrigem.add(aluno.getEscolaOrigem());
		}
		return nomesEscolasOrigem;
	}

	public static Set<String> obterNomesEscolaOrigemAlternativa() {
		Set<String> nomesEscolasOrigem = new TreeSet<>();
		for (Pessoa pessoa : PessoaDAO.obterPessoas()) {
			if (pessoa instanceof Aluno) {
				Aluno aluno = (Aluno) pessoa;
				nomesEscolasOrigem.add(aluno.getEscolaOrigem());
			}
		}
		return nomesEscolasOrigem;
	}

	public static List<Pessoa> obterPessoasOrdemNome() {
		List<Pessoa> pessoas = PessoaDAO.obterPessoas();
		pessoas.sort(Comparator.naturalOrder());
		return pessoas;
	}

	public static List<Pessoa> obterPessoasOrdemNome1() {
		List<Pessoa> pessoas = PessoaDAO.obterPessoas();
		Collections.sort(pessoas);
		return pessoas;
	}

	public static List<Pessoa> obterPessoasOrdemNome2() {
		List<Pessoa> pessoas = PessoaDAO.obterPessoas();
		pessoas.sort(Comparator.comparing(Pessoa::getNome));
		return pessoas;
	}

	public static List<Pessoa> obterPessoasOrdemNome3() {
		List<Pessoa> pessoas = PessoaDAO.obterPessoas();
		Comparator<Pessoa> comparadorPorNome = new Comparator<Pessoa>() {
			@Override
			public int compare(Pessoa pessoa1, Pessoa pessoa2) {
				return pessoa1.getNome().compareTo(pessoa2.getNome());
			}
		};
		Collections.sort(pessoas, comparadorPorNome);
		return pessoas;
	}

	public static List<Pessoa> obterPessoasOrdemNacionalidadeNome() {
		List<Pessoa> pessoas = PessoaDAO.obterPessoas();
		pessoas.sort(Comparator.comparing(Pessoa::getNacionalidade).thenComparing(Pessoa::getNome));
		return pessoas;
	}

	public static List<Pessoa> obterPessoasOrdemNacionalidadeNome1() {
		List<Pessoa> pessoas = PessoaDAO.obterPessoas();
		Comparator<Pessoa> comparadorPorNacionalidadePorNome = new Comparator<Pessoa>() {
			@Override
			public int compare(Pessoa pessoa1, Pessoa pessoa2) {
				int resultado = pessoa1.getNacionalidade().compareTo(pessoa2.getNacionalidade());
				if (resultado == 0) {
					resultado = pessoa1.getNome().compareTo(pessoa2.getNome());
				}
				return resultado;
			}
		};
		Collections.sort(pessoas, comparadorPorNacionalidadePorNome);
		return pessoas;
	}

}
