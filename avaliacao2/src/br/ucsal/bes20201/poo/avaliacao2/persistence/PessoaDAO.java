package br.ucsal.bes20201.poo.avaliacao2.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20201.poo.avaliacao2.domain.Aluno;
import br.ucsal.bes20201.poo.avaliacao2.domain.Pessoa;

public class PessoaDAO {

	private static List<Pessoa> pessoas = new ArrayList<>();

	public static void inserir(Pessoa pessoa) {
		pessoas.add(pessoa);
	}

	public static List<Pessoa> obterPessoas() {
		return new ArrayList<Pessoa>(pessoas);
	}

	public static List<Aluno> obterAlunos() {
		List<Aluno> alunos = new ArrayList<>();
		for (Pessoa pessoa : pessoas) {
			if (pessoa instanceof Aluno) {
				alunos.add((Aluno) pessoa);
			}
		}
		return alunos;
	}

}
