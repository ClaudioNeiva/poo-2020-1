package br.ucsal.bes20201.poo.avaliacao2.tui;

import java.util.Scanner;

import br.ucsal.bes20201.poo.avaliacao2.business.PessoaBO;
import br.ucsal.bes20201.poo.avaliacao2.domain.Pessoa;
import br.ucsal.bes20201.poo.avaliacao2.exception.ValorNaoInformadoException;

public class PessoaTUI {

	private static Scanner scanner = new Scanner(System.in);

	/**
	 * Método para teste das funcionalidades implementadas.
	 * 
	 * Esse método não foi solicitado na avaliação.
	 */
	public static void main(String[] args) {
		cadastrarAluno();
		cadastrarAluno();
		listarPessoasPorNacionalidadeNome();
	}

	public static void cadastrarAluno() {
		String nome;
		String telefone;
		String nacionalidade;
		Integer anoNascimento;
		String escolaOrigem;

		nome = obterTexto("Informe o nome:");
		telefone = obterTexto("Informe o telefone:");
		nacionalidade = obterTexto("Informe a nacionalidade:");
		anoNascimento = obterInteiro("Informe o ano de nascimento:");
		escolaOrigem = obterTexto("Informe a escola de origem:");

		try {
			PessoaBO.cadastrarAluno(nome, telefone, nacionalidade, anoNascimento, escolaOrigem);
		} catch (ValorNaoInformadoException e) {
			System.out.println("Erro:" + e.getMessage());
		}

	}

	public static void listarPessoasPorNacionalidadeNome() {
		System.out.println("Pessoas por nacionalidade e nome:");
		for (Pessoa pessoa : PessoaBO.obterPessoasOrdemNacionalidadeNome()) {
			System.out.println(pessoa);
		}
	}

	private static String obterTexto(String mensagem) {
		System.out.println(mensagem);
		return scanner.nextLine();
	}

	private static Integer obterInteiro(String mensagem) {
		System.out.println(mensagem);
		Integer n = scanner.nextInt();
		scanner.nextLine();
		return n;
	}

}
