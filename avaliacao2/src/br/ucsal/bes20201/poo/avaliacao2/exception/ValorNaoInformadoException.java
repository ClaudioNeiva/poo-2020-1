package br.ucsal.bes20201.poo.avaliacao2.exception;

public class ValorNaoInformadoException extends Exception {

	private static final long serialVersionUID = 1L;

	public ValorNaoInformadoException(String message) {
		super(message);
	}

}
