package br.ucsal.bes20201.poo.avaliacao2.domain;

import br.ucsal.bes20201.poo.avaliacao2.exception.ValorNaoInformadoException;

public abstract class Pessoa implements Comparable<Pessoa> {

	private String nacionalidade;

	private String nome;

	private String telefone;

	public Pessoa(String nome, String telefone, String nacionalidade) {
		this.nome = nome;
		this.telefone = telefone;
		this.nacionalidade = nacionalidade;
	}

	public String getNacionalidade() {
		return nacionalidade;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) throws ValorNaoInformadoException {
		this.telefone = telefone;
	}

	@Override
	public String toString() {
		return "Pessoa [nacionalidade=" + nacionalidade + ", nome=" + nome + ", telefone=" + telefone + "]";
	}

	@Override
	public int compareTo(Pessoa outraPessoa) {
		return nome.compareTo(outraPessoa.nome);
	}

}
