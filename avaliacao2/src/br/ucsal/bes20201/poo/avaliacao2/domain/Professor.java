package br.ucsal.bes20201.poo.avaliacao2.domain;

public class Professor extends Pessoa {

	private TitulacaoEnum titulacao;

	public Professor(String nome, String telefone, String nacionalidade, TitulacaoEnum titulacao) {
		super(nome, telefone, nacionalidade);
		this.titulacao = titulacao;
	}

	public TitulacaoEnum getTitulacao() {
		return titulacao;
	}

	public void setTitulacao(TitulacaoEnum titulacao) {
		this.titulacao = titulacao;
	}

	@Override
	public String toString() {
		return super.toString()+ " - Professor [titulacao=" + titulacao + "]";
	}

}
