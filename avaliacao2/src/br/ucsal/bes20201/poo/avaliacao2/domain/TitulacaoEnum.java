package br.ucsal.bes20201.poo.avaliacao2.domain;

public enum TitulacaoEnum {

	ESPECIALISTA, MESTRE, DOUTOR;

}
