package br.ucsal.bes20201.poo.avaliacao2.domain;

import br.ucsal.bes20201.poo.avaliacao2.exception.ValorNaoInformadoException;

public class Aluno extends Pessoa {

	private Integer anoNascimento;

	private String escolaOrigem;

	public Aluno(String nome, String telefone, String nacionalidade, Integer anoNascimento, String escolaOrigem) throws ValorNaoInformadoException {
		super(nome, telefone, nacionalidade);
		validarTelefone(telefone);
		this.anoNascimento = anoNascimento;
		this.escolaOrigem = escolaOrigem;
	}

	@Override
	public void setTelefone(String telefone) throws ValorNaoInformadoException {
		validarTelefone(telefone);
		super.setTelefone(telefone);
	}

	private void validarTelefone(String telefone) throws ValorNaoInformadoException {
		if (telefone == null || telefone.isEmpty()) {
			throw new ValorNaoInformadoException("O telefone é obrigatório.");
		}
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	public String getEscolaOrigem() {
		return escolaOrigem;
	}

	public void setEscolaOrigem(String escolaOrigem) {
		this.escolaOrigem = escolaOrigem;
	}

	@Override
	public String toString() {
		return super.toString()+ " - Aluno [anoNascimento=" + anoNascimento + ", escolaOrigem=" + escolaOrigem + "]";
	}

}
