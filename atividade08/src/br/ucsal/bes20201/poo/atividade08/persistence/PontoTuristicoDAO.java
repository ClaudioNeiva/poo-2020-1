package br.ucsal.bes20201.poo.atividade08.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20201.poo.atividade08.domain.PontoTuristico;

public class PontoTuristicoDAO {

	private static List<PontoTuristico> pontosTuristicos = new ArrayList<>();
	
	public static void salvar(PontoTuristico pontoTuristico) {
		pontosTuristicos.add(pontoTuristico);
	}

	public static List<PontoTuristico> obterTodos() {
		return pontosTuristicos;
	}
	
}
