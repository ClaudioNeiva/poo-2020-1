package br.ucsal.bes20201.poo.atividade08.business;

import java.util.List;

import br.ucsal.bes20201.poo.atividade08.domain.ChefeCozinha;
import br.ucsal.bes20201.poo.atividade08.domain.Igreja;
import br.ucsal.bes20201.poo.atividade08.domain.Localizacao;
import br.ucsal.bes20201.poo.atividade08.domain.PontoTuristico;
import br.ucsal.bes20201.poo.atividade08.domain.Restaurante;
import br.ucsal.bes20201.poo.atividade08.domain.enums.EstiloArquiteturalEnum;
import br.ucsal.bes20201.poo.atividade08.persistence.PontoTuristicoDAO;

public class PontoTuristicoBO {

	// Não foi solicitado na questão!
	public static void cadastrarPontoTuristico(String nome, Localizacao localizacao) {
		PontoTuristico pontoTuristico = new PontoTuristico(nome, localizacao);
		PontoTuristicoDAO.salvar(pontoTuristico);
	}

	public static void cadastrarIgreja(String nome, Localizacao localizacao, EstiloArquiteturalEnum estiloArquitetural,
			Integer anoConstrucao) {
		Igreja igreja = new Igreja(nome, localizacao, estiloArquitetural, anoConstrucao);
		PontoTuristicoDAO.salvar(igreja);
	}

	public static void cadastrarRestaurante(String nome, Localizacao localizacao, List<String> comidas,
			ChefeCozinha chefeCozinha) {
		Restaurante restaurante = new Restaurante(nome, localizacao, comidas, chefeCozinha);
		PontoTuristicoDAO.salvar(restaurante);
	}

	public static List<PontoTuristico> obterTodos() {
		return PontoTuristicoDAO.obterTodos();
	}

}
