package br.ucsal.bes20201.poo.atividade08.tui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.ucsal.bes20201.poo.atividade08.business.PontoTuristicoBO;
import br.ucsal.bes20201.poo.atividade08.domain.ChefeCozinha;
import br.ucsal.bes20201.poo.atividade08.domain.Localizacao;
import br.ucsal.bes20201.poo.atividade08.domain.PontoTuristico;
import br.ucsal.bes20201.poo.atividade08.domain.enums.EstiloArquiteturalEnum;

public class PontoTuristicoTUI {

	// Não foi solicitado na questão. Foi implementado apenas para ilustrar o uso.
	public static void main(String[] args) {
		carregarDados();
		listar();
	}

	public static void listar() {
		List<PontoTuristico> pontosTuristicos = PontoTuristicoBO.obterTodos();
		for (PontoTuristico pontoTuristico : pontosTuristicos) {
			System.out.println(pontoTuristico);
		}
	}

	// Não foi solicitado na questão. Foi implementado apenas para ilustrar o uso.
	private static void carregarDados() {
		PontoTuristicoBO.cadastrarIgreja("Igreja1", new Localizacao("Rua x", "Bairro y"),
				EstiloArquiteturalEnum.BARROCO, 1830);
		PontoTuristicoBO.cadastrarIgreja("Igreja2", new Localizacao("Rua a", "Bairro t"), EstiloArquiteturalEnum.GOTICO,
				1880);
		PontoTuristicoBO.cadastrarIgreja("Igreja3", new Localizacao("Rua c", "Bairro u"),
				EstiloArquiteturalEnum.RENASCENTISTA, 1850);

		List<String> comidas1 = new ArrayList<>();
		comidas1.add("feijão");
		comidas1.add("arroz");
		ChefeCozinha chefeCozinha1 = new ChefeCozinha("claudio", "Diversos", 1900, "Brasil");
		PontoTuristicoBO.cadastrarRestaurante("Restaurante 1", new Localizacao("Rua z", "Bairro w"), comidas1,
				chefeCozinha1);

		List<String> comidas2 = Arrays.asList("macarrão", "pudim", "bife");
		ChefeCozinha chefeCozinha2 = new ChefeCozinha("antonio", "caseira", 1950, "Alemanha");
		PontoTuristicoBO.cadastrarRestaurante("Restaurante 2", new Localizacao("Rua p", "Bairro b"), comidas2,
				chefeCozinha2);

		PontoTuristicoBO.cadastrarPontoTuristico("Shopping da Bahia", new Localizacao("Rua asddfsa", "Bairro fghfgh"));
	}

}
