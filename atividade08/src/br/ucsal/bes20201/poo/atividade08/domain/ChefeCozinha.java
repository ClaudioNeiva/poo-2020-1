package br.ucsal.bes20201.poo.atividade08.domain;

public class ChefeCozinha {

	private String nome;

	private String especialidade;

	private Integer anoNascimento;

	private String paisOrigem;

	public ChefeCozinha(String nome, String especialidade, Integer anoNascimento, String paisOrigem) {
		super();
		this.nome = nome;
		this.especialidade = especialidade;
		this.anoNascimento = anoNascimento;
		this.paisOrigem = paisOrigem;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	public String getPaisOrigem() {
		return paisOrigem;
	}

	public void setPaisOrigem(String paisOrigem) {
		this.paisOrigem = paisOrigem;
	}

	@Override
	public String toString() {
		return "ChefeCozinha [nome=" + nome + ", especialidade=" + especialidade + ", anoNascimento=" + anoNascimento
				+ ", paisOrigem=" + paisOrigem + "]";
	}

}
