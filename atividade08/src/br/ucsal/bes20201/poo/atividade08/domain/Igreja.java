package br.ucsal.bes20201.poo.atividade08.domain;

import br.ucsal.bes20201.poo.atividade08.domain.enums.EstiloArquiteturalEnum;

public class Igreja extends PontoTuristico {

	private EstiloArquiteturalEnum estiloArquitetural;

	private Integer anoConstrucao;

	public Igreja(String nome, Localizacao localizacao, EstiloArquiteturalEnum estiloArquitetural,
			Integer anoConstrucao) {
		super(nome, localizacao);
		this.estiloArquitetural = estiloArquitetural;
		this.anoConstrucao = anoConstrucao;
	}

	public EstiloArquiteturalEnum getEstiloArquitetural() {
		return estiloArquitetural;
	}

	public void setEstiloArquitetural(EstiloArquiteturalEnum estiloArquitetural) {
		this.estiloArquitetural = estiloArquitetural;
	}

	public Integer getAnoConstrucao() {
		return anoConstrucao;
	}

	public void setAnoConstrucao(Integer anoConstrucao) {
		this.anoConstrucao = anoConstrucao;
	}

	@Override
	public String toString() {
		return "Igreja [estiloArquitetural=" + estiloArquitetural.getDescricaoCompleta() + ", anoConstrucao=" + anoConstrucao + ", toString()="
				+ super.toString() + "]";
	}

}
