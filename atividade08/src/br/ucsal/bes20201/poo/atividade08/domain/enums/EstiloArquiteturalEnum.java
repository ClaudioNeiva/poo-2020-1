package br.ucsal.bes20201.poo.atividade08.domain.enums;

/* 
 * Obs: os atributos (codigo e descricao) não foram solicitados na atividade. 
 * Eles foram utilizados para ilustrar itens de enumeração com atributos.
 */
public enum EstiloArquiteturalEnum {

	GOTICO(101, "Gótico"),

	RENASCENTISTA(450, "Renascentista"),

	BARROCO(300, "Barroco"),

	MODERNO(800, "Moderno");

	private Integer codigo;

	private String descricao;

	private EstiloArquiteturalEnum(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getDescricaoCompleta() {
		return codigo + " - " + descricao;
	}

}
