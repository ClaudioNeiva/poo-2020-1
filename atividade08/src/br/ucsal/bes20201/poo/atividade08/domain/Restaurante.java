package br.ucsal.bes20201.poo.atividade08.domain;

import java.util.List;

public class Restaurante extends PontoTuristico {

	private List<String> comidas;

	private ChefeCozinha chefeCozinha;

	public Restaurante(String nome, Localizacao localizacao, List<String> comidas, ChefeCozinha chefeCozinha) {
		super(nome, localizacao);
		this.comidas = comidas;
		this.chefeCozinha = chefeCozinha;
	}

	public List<String> getComidas() {
		return comidas;
	}

	public void setComidas(List<String> comidas) {
		this.comidas = comidas;
	}

	public ChefeCozinha getChefeCozinha() {
		return chefeCozinha;
	}

	public void setChefeCozinha(ChefeCozinha chefeCozinha) {
		this.chefeCozinha = chefeCozinha;
	}

	@Override
	public String toString() {
		return "Restaurante [comidas=" + comidas + ", chefeCozinha=" + chefeCozinha + ", toString()=" + super.toString()
				+ "]";
	}

}
