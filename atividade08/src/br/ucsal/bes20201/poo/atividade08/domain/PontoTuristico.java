package br.ucsal.bes20201.poo.atividade08.domain;

public class PontoTuristico {

	private String nome;

	private Localizacao localizacao;

	public PontoTuristico(String nome, Localizacao localizacao) {
		super();
		this.nome = nome;
		this.localizacao = localizacao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Localizacao getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(Localizacao localizacao) {
		this.localizacao = localizacao;
	}

	@Override
	public String toString() {
		return "PontoTuristico [nome=" + nome + ", localizacao=" + localizacao + "]";
	}

}
