package br.ucsal.bes20201.poo.atividade01;

import java.util.Scanner;

public class Questao01 {

	private static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		resolverQuestao01();
	}

	private static void resolverQuestao01() {
		Integer nota;
		String conceito;

		nota = obterNota();
		conceito = calcularConceito(nota);
		exbirConceito(conceito);
	}

	private static Integer obterNota() {
		Integer nota;
		System.out.println("Informe um número (0 a 100):");
		nota = scanner.nextInt();
		return nota;
	}

	private static void exbirConceito(String conceito) {
		System.out.println("Conceito=" + conceito);
	}

	private static String calcularConceito(Integer nota) {
		String conceito;
		if (nota <= 49) {
			conceito = "Insuficiente";
		} else if (nota <= 64) {
			conceito = "Regular";
		} else if (nota <= 84) {
			conceito = "Bom";
		} else {
			conceito = "Ótimo";
		}
		return conceito;
	}

}
