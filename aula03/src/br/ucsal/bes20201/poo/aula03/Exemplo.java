package br.ucsal.bes20201.poo.aula03;

public class Exemplo {
	public static void main(String[] args) {

		Aluno aluno1 = new Aluno();
		aluno1.matricula = 123;
		aluno1.nome = "Claudio";
		aluno1.email = "claudio@ucsal.br";
		
		Aluno aluno2 = new Aluno();
		aluno2.matricula = 456;
		aluno2.nome = "Maria";
		aluno2.email = "maria@ucsal.br";
		
		Aluno aluno3 = aluno1;
		aluno3.matricula = 920;
		aluno3.nome = "Pedro";
		aluno3.email = "pedro@ucsal.br";
		
		
		System.out.println("aluno1.nome="+aluno1.nome);
		System.out.println("aluno2.nome="+aluno2.nome);
		System.out.println("aluno3.nome="+aluno3.nome);
	}


}
