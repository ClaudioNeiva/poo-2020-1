package br.ucsal.bes20201.poo.aula15.atividade10.tui;

import java.util.Scanner;

import br.ucsal.bes20201.poo.aula15.atividade10.business.ContaCorrenteBO;
import br.ucsal.bes20201.poo.aula15.atividade10.domain.ContaCorrente;
import br.ucsal.bes20201.poo.aula15.atividade10.exception.RegistroNaoEncontradoException;

public class MovimentacaoTUI {

	private static Scanner sc = new Scanner(System.in);

	public static void sacar() {
		String numAgencia;
		String numConta;
		Double valor;

		System.out.println("Informe o número da agência:");
		numAgencia = sc.nextLine();
		System.out.println("Informe o número da conta:");
		numConta = sc.nextLine();
		System.out.println("Informe o valor:");
		valor = sc.nextDouble();
		sc.nextLine();

		ContaCorrente contaCorrente;
		try {
			contaCorrente = ContaCorrenteBO.obter(numAgencia, numConta);
			/*
			 * Se a conta corrente for encontrada, então, a execução prossegue normalmente;
			 */
			if (contaCorrente.sacar(valor)) {
				System.out.println("Saque realizado. com sucesso na conta de " + contaCorrente.getNomeCorrentista());
			} else {
				System.out.println("Saldo insuficiente!");
			}
		} catch (RegistroNaoEncontradoException e) {
			/*
			 * Se a conta corrente NÃO for encontrada, uma exceção será lançada no
			 * ContaCorrenteDAO, encaminha (throws) pelo ContaCorrenteBO, o fluxo no try é
			 * interrompido e a execução é desviada para dentro de catch.
			 */
			System.out.println(e.getMessage());
		}

	}

	public static void depositar() {

	}

	public static void consultarSaldo() {

	}

	static void emitirExtrato() {

	}
}
