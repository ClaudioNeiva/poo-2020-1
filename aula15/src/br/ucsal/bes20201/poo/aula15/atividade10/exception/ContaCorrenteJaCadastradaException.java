package br.ucsal.bes20201.poo.aula15.atividade10.exception;

public class ContaCorrenteJaCadastradaException extends Exception {

	private static final long serialVersionUID = 1L;

	public ContaCorrenteJaCadastradaException(String message) {
		super(message);
	}

}
