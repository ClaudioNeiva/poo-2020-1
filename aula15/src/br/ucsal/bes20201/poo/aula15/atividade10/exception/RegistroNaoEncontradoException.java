package br.ucsal.bes20201.poo.aula15.atividade10.exception;

public class RegistroNaoEncontradoException extends Exception {

	private static final long serialVersionUID = 1L;

	public RegistroNaoEncontradoException(String message) {
		super(message);
	}

}
