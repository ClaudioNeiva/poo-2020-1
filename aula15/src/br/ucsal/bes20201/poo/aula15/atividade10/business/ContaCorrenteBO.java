package br.ucsal.bes20201.poo.aula15.atividade10.business;

import br.ucsal.bes20201.poo.aula15.atividade10.domain.ContaCorrente;
import br.ucsal.bes20201.poo.aula15.atividade10.exception.ContaCorrenteJaCadastradaException;
import br.ucsal.bes20201.poo.aula15.atividade10.exception.RegistroNaoEncontradoException;
import br.ucsal.bes20201.poo.aula15.atividade10.persistence.ContaCorrenteDAO;

public class ContaCorrenteBO {

	public static void inserir(ContaCorrente contaCorrente) throws ContaCorrenteJaCadastradaException {
		try {
			ContaCorrente contaCorrenteEncontrada = obter(contaCorrente.getNumAgencia(), contaCorrente.getNumConta());
			/*
			 * Se chegou aqui, é porque já existe uma conta corrente e deve ser indicado um
			 * erro para que solicitou a inclusão.
			 */
			throw new ContaCorrenteJaCadastradaException(
					"Conta corrente já cadastrada para o correntista " + contaCorrenteEncontrada.getNomeCorrentista());
		} catch (RegistroNaoEncontradoException e) {
			/*
			 * Se chegou aqui, é porque não existe a conta corrente e, se a conta corrente
			 * não foi encontrada, a mesma deve ser inserida na base de dados.
			 */
			ContaCorrenteDAO.inserir(contaCorrente);
		}
	}

	public static ContaCorrente obter(String numAgencia, String numConta) throws RegistroNaoEncontradoException {
		return ContaCorrenteDAO.obter(numAgencia, numConta);
	}

}
