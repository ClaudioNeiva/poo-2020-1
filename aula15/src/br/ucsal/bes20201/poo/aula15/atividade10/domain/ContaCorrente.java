package br.ucsal.bes20201.poo.aula15.atividade10.domain;

public class ContaCorrente {

	private static final double SALDO_INICIAL = 0d;

	private String numAgencia;

	private String numConta;

	private String nomeCorrentista;

	private Double saldo;

	public ContaCorrente(String numAgencia, String numConta, String nomeCorrentista) {
		this.numAgencia = numAgencia;
		this.numConta = numConta;
		this.nomeCorrentista = nomeCorrentista;
		saldo = SALDO_INICIAL;
	}

	// FIXME Modificar para void e tratar a falta de saldo como uma exceção.
	public Boolean sacar(Double valor) {
		if (saldo >= valor) {
			saldo -= valor;
			return true;
		}
		// FIXME Lançar uma exceção aqui!
		return false;
	}

	public String getNumAgencia() {
		return numAgencia;
	}

	public String getNumConta() {
		return numConta;
	}

	public String getNomeCorrentista() {
		return nomeCorrentista;
	}

	public void setNomeCorrentista(String nomeCorrentista) {
		/*
		 * Posso escreve aqui regras de negócio que validam o nomeCorrentista antes de
		 * atualizar o atributo.
		 */
		this.nomeCorrentista = nomeCorrentista;
	}
}
