package br.ucsal.bes20201.poo.aula15.atividade10.tui;

public enum MenuEnum {

	ABERTURA_CONTA(1, "Abertura de conta"),

	SAQUE(2, "Saque"),

	DEPOSITO(3, "Depósito"),

	SAIR(9, "Sair");

	private Integer codigo;

	private String descricao;

	private MenuEnum(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public String obterDescricaoDetalhada() {
		return codigo + " - " + descricao;
	}

	public static MenuEnum valueOfCodigo(Integer opcaoInteger) {
		for (MenuEnum menu : values()) {
			if (opcaoInteger.equals(menu.codigo)) {
				return menu;
			}
		}
		/*
		 * FIXME Quando aprendermos exceção, vamos lançar uma IllegalArgumentException
		 * aqui!
		 */
		return null;
	}

}
