package br.ucsal.bes20201.poo.aula15.atividade10.persistence;

import br.ucsal.bes20201.poo.aula15.atividade10.domain.ContaCorrente;
import br.ucsal.bes20201.poo.aula15.atividade10.exception.RegistroNaoEncontradoException;

public class ContaCorrenteDAO {

	private static final int QTD_MAX = 1000;

	private static ContaCorrente[] contasCorrente = new ContaCorrente[QTD_MAX];
	private static int qtd = 0;

	public static void inserir(ContaCorrente contaCorrente) {
		contasCorrente[qtd] = contaCorrente;
		qtd++;
	}

	public static ContaCorrente obter(String numAgencia, String numConta) throws RegistroNaoEncontradoException {
		for (int i = 0; i < qtd; i++) {
			if (numAgencia.equals(contasCorrente[i].getNumAgencia())
					&& numConta.equals(contasCorrente[i].getNumConta())) {
				return contasCorrente[i];
			}
		}
		/*
		 * Quando eu chego neste ponto do código, significa que não foi encontrada
		 * nenhum conta corrente para a numAgencia e numConta passados como parâmetro.
		 */
		throw new RegistroNaoEncontradoException("ContaCorrente não encontrada para agência " + numAgencia + " conta " + numConta);
	}

}
